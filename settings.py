import os

from peewee import *

class Settings:
    DB_DIR = os.path.abspath("db")

    BLOCKCHAIN_FILE = os.path.join(DB_DIR, "/home/thedoctor/3D-B(itch)/BigBossBlockchain/db/blockchain.db")
    BLOCKCHAIN_DB = SqliteDatabase(BLOCKCHAIN_FILE)