"""
An action of changing someone's privileges on the blockchain.

:author: Thogs
:date: 25.3.18
"""

import json
import logging
from peewee import *
from settings import Settings

from models.action import Action
from models.role import Role

logger = logging.getLogger("ActionRolechange")
db = Settings.BLOCKCHAIN_DB


class ActionAddRole(Action, Model):

    action_type = IntegerField(default=1)
    recipient_role = ForeignKeyField(Role)

    def from_json(self, data: dict):  # -> ActionRolechange
        try:
            self._from_json_base(data)

            self.recipient_role = Role()
            self.recipient_role.from_json(data["recipient_role"])

        except:
            error_msg = "Invalid JSON data - Couldn't create ActionRolechange"
            logger.error(error_msg)
            raise ValueError(error_msg)
