"""
A representation of a block.

:author: Thogs
:date: 25.3.18
"""

import json
import logging
from peewee import *

from models.signature import Signature
from settings import Settings

logger = logging.getLogger("Block")
db = Settings.BLOCKCHAIN_DB


class Block(Model):
    index = IntegerField()
    prev_hash = CharField()
    timestamp = IntegerField()
    _actions = CharField()
    hash = CharField()
    _signatures = CharField()
    final_hash = CharField()
    final_signature = ForeignKeyField(Signature)

    def get_actions(self) -> list:
        return json.loads(self._actions)

    def get_signatures(self) -> list:
        signature_list = []
        for signature_str in json.loads(self._signatures):
            signature = Signature()
            signature.from_json(signature_str)
            signature_list.append(signature)

        return signature_list

    class Meta:
        database = db
        primary_key = False
