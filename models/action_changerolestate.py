"""
An action of changing someone's role_state on the blockchain.

:author: Thogs
:date: 25.3.18
"""

import json
import logging
from peewee import *
from settings import Settings

from models.action import Action

logger = logging.getLogger("ActionChangeRolestate")
db = Settings.BLOCKCHAIN_DB


class ActionChangeRolestate(Action, Model):

    action_type = IntegerField(default=3)
    role_id = IntegerField()
    role_state = CharField()

    def _create_hash(self):
        data = self._create_hash_super()

        data["role_id"] = self.role_id
        data["role_state"] = self.role_state

        return Crypto.sha256_json(data)

    def from_json(self, data: dict):  # -> ActionChangeRolestate
        try:
            self._from_json_base(data)

            self.role_id = data["role_id"]
            self.role_state = data["role_state"]

        except:
            error_msg = "Invalid JSON data - Couldn't create ActionChangeRolestate"
            logger.error(error_msg)
            raise ValueError(error_msg)

    def to_json(self) -> str:
        data = self._to_json_base()

        data["role_id"] = self.role_id
        data["role_state"] = self.role_state

        return json.dumps(data, sort_keys=True)
