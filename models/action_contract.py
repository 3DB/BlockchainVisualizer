"""
An action of publishing a contract.

:author: Thogs
:date: 30.4.18
"""

import json
import logging
from peewee import *
from settings import Settings

from models.signature import Signature
from models.action import Action
from models.contract import Contract

logger = logging.getLogger("ActionContract")
db = Settings.BLOCKCHAIN_DB


class ActionContract(Action, Model):

    action_type = IntegerField(default=2)
    contract = ForeignKeyField(Contract)

    def from_json(self, data: dict):  # -> ActionContract
        try:
            self._from_json_base(data)

            self.contract = Contract()
            self.contract.from_json(data["contract"])

        except:
            error_msg = "Invalid JSON data - Couldn't create ActionContract"
            logger.error(error_msg)
            raise ValueError(error_msg)

    def to_json(self) -> str:
        data = self._to_json_base()

        data["contract"] = self.contract.to_json()

        return json.dumps(data, sort_keys=True)

    def save(self, force_insert=False, only=None):
        self.contract.save()
        Model.save(self, force_insert=force_insert, only=only)
