from hashlib import sha256
from time import sleep
import datetime
import json

from models.block import Block
from models.action_changerolestate import ActionChangeRolestate
from models.action_contract import ActionContract
from models.action_addrole import ActionAddRole

COLOR = "\033[37m"


class Main:

    def __init__(self):
        last_len = 0
        while True:
            blocks = Block.select()
            if len(blocks) > last_len:
                for i in range(len(blocks) - last_len):
                    if blocks[i + last_len].index != 0:
                        print(COLOR + 16 * " " + "▲\n"
                              + 16 * " " + "|\n"
                              + 16 * " " + "|"
                              + "\033[0m")
                        print()
                    self.print_block(blocks[i + last_len])
                last_len = len(blocks)

            sleep(0.5)

    def print_block(self, block: Block):
        lines = [""]
        lines.append("Index: " + str(block.index))
        lines.append("Timestamp: " + self.time_to_str(block.timestamp))
        lines.append("Actions: <" + str(len(block.get_actions())) + ">")
        lines.append("Signatures: <" + str(len(block.get_signatures())) + ">")
        lines.append("Previous Hash: " + self.short_string(block.prev_hash))
        lines.append("Hash: " + self.short_string(block.hash))
        lines.append("Final Hash: " + self.short_string(block.final_hash))
        lines.append("Node ID: " + str(block.final_signature.role_id))
        lines.append("Final Signature: " + self.short_string(block.final_signature.signature))
        lines.append("")

        # Longest line
        longest = 0
        for line in lines:
            if len(line) > longest:
                longest = len(line)

        # Left side
        for i in range(len(lines) - 2):
            lines[i + 1] = "| " + lines[i + 1] + (longest - len(lines[i + 1])) * " " + " |"

        # Beginning & ending line
        line = "+" + (longest + 2) * "-" + "+"
        lines[0] = line
        lines[-1] = line

        # Actions
        actions = block.get_actions()
        for action in actions:
            action = self.action_from_json(action)

            # Add filling stuff
            for i in range(len(lines)):
                if int(len(lines) / 2) != i:
                    lines[i] = lines[i] + 5 * " "
                    if i != 0 and i != len(lines) - 1:
                        lines[i] = lines[i] + "| "
                else:
                    lines[i] = lines[i] + " ==> | "

            # End block line
            end_line = 0

            # AddRole
            if action.action_type == 1:
                lines[1] = lines[1] + "Action: AddRole"
                lines[2] = lines[2] + "Donor ID: " + str(action.signature.role_id)
                lines[3] = lines[3] + "Pubkey: " + self.short_string(action.recipient_role.pubkey)
                lines[4] = lines[4] + "Role Level: " + str(action.recipient_role.role_level)
                lines[5] = lines[5] + "Role ID: " + str(action.recipient_role.role_id)
                lines[6] = lines[6] + "Address: " + action.recipient_role.address
                lines[7] = lines[7] + "CS Server: " + action.recipient_role.cs_server
                lines[8] = lines[8] + "Role State: " + action.recipient_role.role_state
                lines[9] = lines[9] + "Action ID: " + str(action.recipient_role.cur_action_id)
                end_line = 10

            # AddContract
            elif action.action_type == 2:
                lines[1] = lines[1] + "Action: AddContract"
                lines[2] = lines[2] + "Additions: " + str(action.contract.additions)
                lines[3] = lines[3] + "Price per kWh: " + str(action.contract.price_per_kwh)
                lines[4] = lines[4] + "Station ID: " + str(action.contract.station_id)
                lines[5] = lines[5] + "Start Timestamp: " + self.time_to_str(action.contract.start_timestamp)
                lines[6] = lines[6] + "End Timestamp: " + self.time_to_str(action.contract.end_timestamp)
                lines[7] = lines[7] + "Charging Amount: " + str(action.contract.charging_amount)
                lines[8] = lines[8] + "User Alias: " + self.short_string(action.contract.user_signature.alias)
                lines[9] = lines[9] + "User Signature: " + self.short_string(action.contract.user_signature.contract_signature)
                end_line = 10

            # ChangeRolestate
            elif action.action_type == 3:
                lines[1] = lines[1] + "Action: ChangeRolestate"
                lines[2] = lines[2] + "Admin ID: " + str(action.signature.role_id)
                lines[3] = lines[3] + "Role ID: " + str(action.role_id)
                lines[4] = lines[4] + "Role State: " + action.role_state
                end_line = 5

            # Longest line
            for line in lines:
                if len(line) > longest:
                    longest = len(line)

            # Fill lines
            for i in range(len(lines) - end_line - 1):
                lines[i + end_line] = lines[i + end_line] + (longest - len(lines[0]) - 1) * " "

            # Top/bot bar
            line = "+" + (longest - len(lines[0]) + 1) * "-"
            lines[0] = lines[0] + line
            lines[-1] = lines[-1] + line

            # Right side
            for i in range(len(lines)):
                if i == 0 or i == len(lines) - 1:
                    lines[i] = lines[i] + "+"
                else:
                    lines[i] = lines[i] + (longest - len(lines[i]) + 1) * " " + " |"

        # Print it!
        for line in lines:
            print(COLOR + line + "\033[0m")

        print()

    def short_string(self, text: str) -> str:
        text = text.strip()
        if len(text) <= 8:
            return text

        return text[:4] + ".." + text[-4:]

    def time_to_str(self, t: int) -> str:
        return str(datetime.datetime.fromtimestamp(t).strftime("%d/%m/%Y %H:%M:%S"))

    def action_from_json(self, json_data: str):
        data = json.loads(json_data)
        action_type = data["action_type"]

        action = None

        if action_type == 1:
            action = ActionAddRole()
            action.from_json(data)

        elif action_type == 2:
            action = ActionContract()
            action.from_json(data)

        elif action_type == 3:
            action = ActionChangeRolestate()
            action.from_json(data)

        return action

    def sha256_text(self, text: str) -> str:
        return sha256(text.encode("utf-8")).hexdigest()

if __name__ == "__main__":
    Main()